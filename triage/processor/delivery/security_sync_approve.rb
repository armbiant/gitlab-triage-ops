# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class SecuritySyncApprove < Processor
    react_to 'merge_request.open', 'merge_request.unapproved', 'merge_request.unapproval', 'merge_request.update', 'merge_request.reopen'

    def applicable?
      event.from_gitlab_org? &&
        event.from_security_fork? &&
        event.source_branch_is?('master') &&
        event.target_branch_is_main_or_master?
    end

    def documentation
      <<~TEXT
        This processor approves the security->canonical merge requests for
        the default branch.
      TEXT
    end

    def process
      comment = <<~MARKDOWN.chomp
        This merge request is a security->canonical sync.

        All the changes included have already been reviewed by the releavant maintainers and AppSec on our security mirror.

        /approve
      MARKDOWN

      add_comment(comment, append_source_link: true)
    end
  end
end
