# frozen_string_literal: true

require_relative 'triage/job/keep_cache_warm_job'
require_relative 'triage/rack/app'

# Warm the cache immediately
Triage::KeepCacheWarmJob.perform_async

run Triage::Rack::App.app
