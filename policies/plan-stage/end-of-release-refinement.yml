resource_rules:
  issues:
    rules:
      - name: End of release refinement
        conditions:
          # Make sure we have at least one closed issue at 12.4
          # so it can generate a summary. (no issue no summary!)
          # This: https://gitlab.com/gitlab-org/plan/issues/10
          state: closed
          milestone: '12.4'
        limits:
          most_recent: 1
        actions:
          summarize:
            title: |
              #{Date.today.iso8601} - end of release refinement
            summary: |
              Use of the ~Deliverable label helps us communicate clearly to
              team-members and the wider community what our priorities are for
              the milestone. _Consistent_ use ensures our Retrospectives and
              [Say/Do Ratios] are accurate.

              As the current release is nearly over, please take the following
              actions:

              1. Review [this board] and filter by the current milestone.
                 Move any non-Deliverables in
                 ~"workflow::ready for development" and leftwards to future
                 milestones or the backlog as appropriate. The remaining issues
                 will be moved automatically and labeled with
                 ~missed-deliverable, so they show up in the Retrospective.
              2. Review ~"workflow::blocked" issues to ensure there is a
                 clear path to being unblocked.
              3. Review issues in the [upcoming milestone].
                 * **Ideally**, add the ~Deliverable label to everything committed to
                   in your planning issue for this milestone. Add ~Stretch to
                   anything else.
                 * **At a minimum**, consider adding the ~Deliverable label to
                   everything in ~"workflow::ready for development" and
                   rightwards, and ~Stretch to everything in
                   ~"workflow::planning breakdown" and leftwards.

              [this board]: https://gitlab.com/groups/gitlab-org/-/boards/1226305?not%5Blabel_name%5D%5B%5D=workflow%3A%3Adesign
              [upcoming milestone]: https://gitlab.com/groups/gitlab-org/-/boards/1226305?not[label_name][]=workflow%3A%3Adesign&label_name[]=devops%3A%3Aplan&milestone_title=Upcoming
              [Say/Do Ratios]: https://app.periscopedata.com/app/gitlab/658030/Say-Do-Ratios
              /assign @donaldcook @kushalpandya @johnhope @blabuschagne
              /due 20
              /label ~"Plan stage refinement"
